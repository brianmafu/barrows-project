Barrows Project Management System

Installation
1. git clone https://gitlab.com/brianmafu/barrows-project.git
2. Create virtual environment: eg. django-1.6.5
3. Activate virtual environment
4. cd into barrows-project/barrows_project
5. Install Packages: pip install -r requirements.txt
6. Run Syncdb ( Defaults to using sqlite3 database): python manage.py syncdb --noinitial
7. Syncdb command will  create a Default 'Super User': Follow the prompts
6. Start Up App Server for Project: python runserver 0.0.0.0:portnumber
7. Go Browser and enter address and port number specified in Step 6