# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponse, HttpResponseBadRequest, StreamingHttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings as django_settings
from django.shortcuts import render, redirect, get_object_or_404, Http404
from barrows_project.decorators import ajax_required
from models import Client
from forms import ClientForm

import json

CLIENT_NUM_PAGES = 10

# Views
# All Clients Summary View
@login_required
def all_clients(request):    
    all_clients =  Client.objects.all()
    paginator = Paginator(all_clients, CLIENT_NUM_PAGES)
    page = 1
    if "page" in request.GET:
        page = int(request.GET.get("page"))
    clients = paginator.page(page)
    from_client = -1    
    if clients:
        from_client = clients[0].id     
    return render(request, 'clients/summary_clients.html', {
        'clients': clients,
        'from_client': from_client, 
        'page': 1,
        })

# Render Add New Client Form
@login_required
def render_add_client_form(request):
    if request.method == 'GET':
        form = ClientForm()
        return render(request, 'clients/add_client.html', {'form': form})

# Render Edit Client Form
@login_required
def render_edit_client_form(request, id=None):
    if request.method == 'GET':
        client = Client.objects.get(id=id)
        form = ClientForm(initial={"client_name": client.client_name,"contact_name": client.contact_name, "contact_number": client.contact_number})
        form.fields['client_name'].widget.attrs['readonly'] = True
        return render(request, 'clients/edit_client.html', {'form': form, "client_id": client.id})

# Ajax Views
# Add Client Ajax View
@login_required
@ajax_required
def add_client(request):
    data = {}    
    if request.method == 'POST':
        form = ClientForm(request.POST)               
        if form.is_valid():               
            client = Client()
            client.client_name = form.cleaned_data.get('client_name')
            client.contact_name = form.cleaned_data.get('contact_name')
            client.contact_number = form.cleaned_data.get('contact_number')
            client.save()                
            confirm_msg = 'Client Added to System: Client <a href="/clients/edit-form/%s">here</a> to edit' % client.id
            data = {'confirm_msg': confirm_msg}
        else:
            data = []
            for k, v in form.errors.items():
                data.append({'element': k, 'error_message': v})    
    response = HttpResponse()
    response['Content-type'] = "application/json"
    response.write(json.dumps(data))
    return response

# Edit Client Ajax View
@login_required
@ajax_required
def edit_client(request, id=None):
    data = {}    
    if request.method == 'POST':
        form = ClientForm(request.POST)
        if form.is_valid():                          
            client = Client.objects.get(id=id)
            client.client_name = form.cleaned_data.get('client_name')
            client.contact_name = form.cleaned_data.get('contact_name')
            client.contact_number = form.cleaned_data.get('contact_number')
            client.save()                
            confirm_msg = 'Client Information Updated'
            data = {'confirm_msg': confirm_msg}
        else:
            data = []
            for k, v in form.errors.items():
                data.append({'element': k, 'error_message': v})
            
        response = HttpResponse()
        response['Content-type'] = "application/json"
        response.write(json.dumps(data))
        return response 

# Delete Client Ajax View
@login_required
@ajax_required
def delete_client(request, id):
    print "this was execute"
    if request.method == 'GET':
        try:
            Client.objects.get(id=id).delete()
            confirm_msg = 'Client Deleted From System'
            data = {'confirm_msg': confirm_msg}
            response = HttpResponse()
            response['Content-type'] = "application/json"
            response.write(json.dumps(data))
            return response 
        except:
            return HttpResponseBadRequest()




