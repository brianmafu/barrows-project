# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponse, HttpResponseBadRequest, StreamingHttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings as django_settings
from django.shortcuts import render, redirect, get_object_or_404, Http404
from barrows_project.decorators import ajax_required
from barrows_project.clients.models import Client
from models import Project
from forms import ProjectForm

import json

PROJECT_NUM_PAGES = 10
# Views
# Summary Page of all Projects
@login_required
def all_projects(request):    
    all_projects =  Project.objects.all()
    paginator = Paginator(all_projects, PROJECT_NUM_PAGES)
    page = 1
    if "page" in request.GET:
        page = int(request.GET.get("page"))
    projects = paginator.page(page)
    from_project = -1
    if projects:
        from_project = projects[0].id
     	
    return render(request, 'projects/summary_projects.html', {
        'projects': projects,
        'from_project': from_project, 
        'page': 1,
        })

# Rendering of add new project form
@login_required
def render_add_project_form(request):
    if request.method == 'GET':
        form = ProjectForm()
        return render(request, 'projects/add_project.html', {'form': form})

# Rendering of edit project form
@login_required
def render_edit_project_form(request, id=None):
    if request.method == 'GET':
        project = Project.objects.get(id=id)
        form = ProjectForm(initial={"project_name": project.project_name,"project_status": project.project_status, "client": project.client})
        form.fields['project_name'].widget.attrs['readonly'] = True
        return render(request, 'projects/edit_project.html', {'form': form, "project_id": project.id})

# Ajax Views
# Add Project Ajax
@login_required
@ajax_required
def add_project(request):
    data = {}    
    if request.method == 'POST':
        form = ProjectForm(request.POST)               
        if form.is_valid():               
            project = Project()
            project.project_name = form.cleaned_data.get('project_name')
            project.project_status = form.cleaned_data.get('project_status')
            project.client = form.cleaned_data.get('client')
            project.save()                
            confirm_msg = 'Project Added to System: Project <a href="/projects/edit-form/%s">here</a> to edit' % project.id
            data = {'confirm_msg': confirm_msg}
        else:
            data = []
            for k, v in form.errors.items():
                data.append({'element': k, 'error_message': v})    
    response = HttpResponse()
    response['Content-type'] = "application/json"
    response.write(json.dumps(data))
    return response

# Edit Project View
@login_required
@ajax_required
def edit_project(request, id=None):
    data = {}
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():                          
            project = Project.objects.get(id=id)
            project.project_name = form.cleaned_data.get('project_name')
            project.project_status = form.cleaned_data.get('project_status')
            project.client = form.cleaned_data.get('client')
            project.save()                
            confirm_msg = 'Project Information Updated'
            data = {'confirm_msg': confirm_msg}
        else:
            data = []
            for k, v in form.errors.items():
                data.append({'element': k, 'error_message': v})
            
        response = HttpResponse()
        response['Content-type'] = "application/json"
        response.write(json.dumps(data))
        return response

# Delete Project Ajax View
@login_required
@ajax_required
def delete_project(request, id):    
    if request.method == 'GET':
        try:
            Project.objects.get(id=id).delete()
            confirm_msg = 'Project Deleted From System'
            data = {'confirm_msg': confirm_msg}
            response = HttpResponse()
            response['Content-type'] = "application/json"
            response.write(json.dumps(data))
            return response 
        except:
            return HttpResponseBadRequest()




