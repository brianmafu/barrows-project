from django import forms
from models import Project, PROJECT_STATUS

# Project Form
class ProjectForm(forms.ModelForm):    
    project_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}), 
        max_length=50,
        required=True)

    class Meta:
        model = Project
        fields = ['project_name', 'project_status', 'client']